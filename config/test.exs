use Mix.Config

config :ex_api, apis: %{
  ConfigApi => [
    ConfigApi.FirstImpl, ConfigApi.SecondImpl,
    ConfigApi.DifferentModuleName
  ],
  EmptyConfigApi => [],
  NoDefaultImplConfigApi => [NoDefaultImplConfigApi.FirstImpl]
},
default_impls: %{ConfigApi => {:sample, :first_impl}},
default_groups: %{EmptyConfigApi => :sample}
