use Mix.Config

file_name = "#{Mix.env}.exs"
if File.exists?("config/" <> file_name), do: import_config file_name
