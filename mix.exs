defmodule ExApi.Mixfile do
  use Mix.Project

  def application, do: [mod: {ExApi.Application, []}]

  def project, do: [
    app: :ex_api,
    build_embedded: Mix.env == :prod,
    deps: [
      {:credo, "~> 0.8", only: :dev, runtime: false},
      {:excoveralls, "~> 0.7", only: :test},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false},
      {:phoenix_pubsub, "~> 1.0"},
    ],
    description: "Library for creating and managing apis and their implementations",
    docs: [
      extras: [
        "README.md": [filename: "ex_api", title: "ExApi"],
      ],
      main: "ex_api"
    ],
    elixir: "~> 1.5",
    package: [
      licenses: ["Apache 2.0"],
      links: %{"GitLab" => "https://gitlab.com/ex-open-source/ex_api"},
      maintainers: ["Tomasz Marek Sulima"],
      name: :ex_api,
    ],
    preferred_cli_env: [
      "coveralls": :test,
      "coveralls.detail": :test,
      "coveralls.post": :test,
      "coveralls.html": :test
    ],
    source_url: "https://gitlab.com/ex-open-source/ex_api",
    start_permanent: Mix.env == :prod,
    test_coverage: [tool: ExCoveralls],
    version: "1.0.0-rc.2",
  ]
end
