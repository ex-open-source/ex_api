defmodule ExApi.MultipleNodesTest do
  use ExUnit.Case

  alias ExApi.Cluster

  @def_api_and_impl """
  import ExApi.Api
  def_api MyApi do
    def_impl :fallback_impl, do: nil
    def_impl :my_impl, do: nil
  end
  """

  test "multiple_nodes" do
    {:ok, _pid} = ExApi.start_link
    {:ok, all_apis} = ExApi.all
    for api <- all_apis, do: ExApi.unregister_api(api)
    node_names = [:first, :second]
    prepare_nodes(node_names)
    [first_node_name | rest_node_names] = node_names
    actions_and_checks = [
      {:register_api, [MyApi], :get, [MyApi]},
      {:register_fallback_impl, [MyApi.FallbackImpl], :get_fallback_impl, [MyApi]},
      {:register_impl, [MyApi.MyImpl], :get_impl, [MyApi.MyImpl]},
      {:set_default_impl, [MyApi.MyImpl], :get, [MyApi]},
      {:unregister_fallback_impl, [MyApi], :get_fallback_impl, [MyApi]},
      {:unregister_impl, [MyApi.MyImpl], :get_impls, [MyApi]},
      {:unregister_api, [MyApi], :all, []},
    ]
    for {action_name, action_args, check_name, check_args} <- actions_and_checks do
      {:ok, _pid} = Cluster.block_call(first_node_name, ExApi, action_name, action_args)
      assert_equal_results(first_node_name, rest_node_names, check_name, check_args)
    end
  end

  defp prepare_nodes(node_names) do
    Code.eval_string(@def_api_and_impl)
    Cluster.spawn(node_names)
    connected_nodes_count = Enum.count(node_names)
    for node_name <- node_names do
      assert connected_nodes_count == node_name |> Cluster.block_call(Node, :list, []) |> Enum.count
      Cluster.block_call(node_name, Code, :eval_string, [@def_api_and_impl])
    end
  end

  defp assert_equal_results(first_node_name, rest_node_names, function_name, args) do
    Process.sleep(100)
    delete_pid = function_name == :get
    {:ok, first_node_block_call_result} = Cluster.block_call(first_node_name, ExApi, function_name, args)
    first_node_result =
      if delete_pid, do: Map.delete(first_node_block_call_result, :pid), else: first_node_block_call_result
    for node_name <- rest_node_names do
      {:ok, node_block_call_result} = Cluster.block_call(node_name, ExApi, function_name, args)
      node_result = if delete_pid, do: Map.delete(node_block_call_result, :pid), else: node_block_call_result
      assert first_node_result == node_result
    end
    {:ok, current_node_apply_result} = apply(ExApi, function_name, args)
    current_node_result =
      if delete_pid, do: Map.delete(current_node_apply_result, :pid), else: current_node_apply_result
    assert first_node_result == current_node_result
  end
end
