defmodule ExApiTest do
  use ExUnit.Case

  alias ExApi.{Error, ValidationHelper}
  alias ExApi.Error.{
    FeatureNotImplementedError,
    FeatureNotSupportedError,
    ImplementationNotFoundError
  }
  alias Phoenix.PubSub

  require ExApiFixtures

  test "config" do
    {:ok, pid} = ExApi.start_link
    {:ok, all_apis} = ExApi.all
    assert Enum.count(all_apis) == 3
    ExApiFixtures.test_data
    |> Enum.with_index
    |> Enum.map(&do_config_test(&1, all_apis))
    unregister_all(pid)
  end

  defp do_config_test({{module, impls_count, group, impl_id}, index}, all_apis) do
    api = Enum.fetch!(all_apis, index)
    assert api == get_expected_api(module)
    assert {:ok, api} == ExApi.check_api(module)
    assert api.module == module
    {:ok, groups} = ExApi.get_impls(module)
    for {group, impls} <- groups do
      assert Enum.count(impls) == impls_count[group]
    end
    result = ExApi.get_impl(module, {group, impl_id})
    if is_nil(group) and is_nil(impl_id) do
      {:error, error} = result
      assert ImplementationNotFoundError == error.__struct__
    else
      {:ok, impl} = result
      assert result == ExApi.check_api_impl(api, impl)
      assert result == ExApi.check_impl(impl)
      assert result == ExApi.get_default_impl(module)
    end
  end

  defp get_expected_api(module) do
    file_name = Macro.underscore(module)
    ExApiFixtures.expected_data("config", [file_name])
  end

  test "dynamic" do
    {:ok, pid} = ExApi.start_link
    PubSub.subscribe(:ex_api, "events")
    ExApiFixtures.load_test_fixture
    {:ok, first_api_list} = ExApi.all
    assert Enum.count(first_api_list) == 3
    Enum.each(ExApiFixtures.test_data, &do_dynamic(&1, pid))
    {:ok, second_api_list} = ExApi.all
    assert Enum.count(second_api_list) == 4
    unregister_all(pid)
  end

  defp do_dynamic({id, func_name, args}, pid) do
    assert {:ok, pid} == apply(ExApi, func_name, args)
    file_name = Atom.to_string(id)
    expected_message = ExApiFixtures.expected_data("dynamic", ["message", file_name])
    expected_result = ExApiFixtures.expected_data("dynamic", ["result", file_name])
    pid = GenServer.whereis(DynamicApi)
    expected_message_with_pid = add_pid(func_name, pid, expected_message)
    assert_receive ^expected_message_with_pid
    assert {:ok, %{expected_result | pid: pid}} == ExApi.get(DynamicApi)
  end

  defp add_pid(:register_api, pid, expected_message), do: put_in(expected_message.data.pid, pid)
  defp add_pid(:set_default_impl, pid, expected_message), do: put_in(expected_message.api.pid, pid)
  defp add_pid(_func_name, _pid, expected_message), do: expected_message

  test "errors" do
    {:ok, pid} = ExApi.start_link
    Enum.each(ExApiFixtures.test_data, &do_test_error/1)
    unregister_all(pid)
  end

  defp do_test_error({function_name, data, error_module}) do
    error = Error.prepare(error_module, data)
    assert {:error, error} == apply(ExApi, function_name, Keyword.values(data))
  end
  defp do_test_error({function_name, data, error_module, extra_data}) do
    error = Error.prepare(error_module, extra_data)
    assert {:error, error} == apply(ExApi, function_name, data)
  end

  test "extra_checks" do
    {:ok, pid} = ExApi.start_link
    api_module = ConfigApi
    impl_module = ConfigApi.FirstImpl
    assert {:ok, :supported} == ExApi.get_feature_support(impl_module, {:another_feature_name, 0})
    {:ok, api} = ExApi.get(api_module)
    {:ok, impl} = ExApi.get_impl(impl_module)
    {:ok, ^api_module} = ValidationHelper.with_api_module_check(api, fn module -> {:ok, module} end)
    {:ok, ^impl_module} = ValidationHelper.with_impl_module_check(impl, fn module -> {:ok, module} end)
    unregister_all(pid)
  end

  test "implementation" do
    {:ok, pid} = ExApi.start_link
    {:ok, config_api} = ExApi.get(ConfigApi)
    {:ok, first_impl} = ExApi.get_impl(config_api, {:sample, :first_impl})
    {:ok, second_impl} = ExApi.get_impl(config_api, {nil, :second_impl})
    feature = config_api.features[{:feature_name, 1}]
    data = [api: config_api, feature: feature]
    info = [
      {first_impl, FeatureNotSupportedError},
      {second_impl, FeatureNotImplementedError}
    ]
    for {impl, error} <- info do
      error_struct = Error.prepare(error, [{:implementation, impl} | data])
      result =
        config_api.module.feature_for({impl.group, impl.id}, feature.name, ["test"], fn _ -> nil end, fn error ->
          assert error == error_struct
        end)
      assert result == {:ok, config_api.pid}
    end
    Process.sleep(1)
    unregister_all(pid)
  end

  defp unregister_all(pid) do
    {:ok, apis} = ExApi.all
    apis_count = Enum.count(apis)
    for {api, api_index} <- Enum.with_index(apis) do
      {:ok, groups} = ExApi.get_impls(api.module)
      for {group, impls} <- groups do
        impls_count = Enum.count(impls)
        for {{_impl_id, impl}, impl_index} <- Enum.with_index(impls) do
          assert {:ok, pid} == ExApi.unregister_impl(impl.module)
          {:ok, new_groups} = ExApi.get_impls(api.module)
          assert impls_count - (impl_index + 1) == Enum.count(new_groups[group])
        end
      end
      empty_groups =
        groups
        |> Enum.map(fn {key, _value} -> {key, %{}} end)
        |> Enum.into(%{})
      assert ExApi.get_impls(api.module) == {:ok, empty_groups}
      assert {:ok, pid} == ExApi.unregister_api(api.module)
      {:ok, new_apis} = ExApi.all
      assert apis_count - (api_index + 1) == Enum.count(new_apis)
    end
    assert ExApi.all == {:ok, []}
    assert :ok == ExApi.stop
  end
end
