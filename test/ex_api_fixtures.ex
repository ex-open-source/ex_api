defmodule ExApiFixtures do
  alias __MODULE__.Errors.Generator

  @fixtures_path "test/ex_api_fixtures"

  def error_for(args), do: {:error, apply(Generator, :error_for, args)}

  def expected_data(test_name, parts) do
    path = Path.join([@fixtures_path, test_name, "expected" | parts]) <> ".ex"
    {result, []} = Code.eval_file(path)
    result
  end

  defmacro load_test_fixture do
    {name, _arity} = __CALLER__.function
    quote do
      fixtures_path = unquote(@fixtures_path)
      name = unquote(name)
      "test " <> test_name = Atom.to_string(name)
      [fixtures_path, test_name, test_name <> ".ex"]
      |> Path.join
      |> Code.load_file
    end
  end

  defmacro test_data do
    {name, _arity} = __CALLER__.function
    quote do
      fixtures_path = unquote(@fixtures_path)
      name = unquote(name)
      "test " <> test_name = Atom.to_string(name)
      {result, _bindings} =
        [fixtures_path, test_name, "data.ex"]
        |> Path.join
        |> Code.eval_file
      result
    end
  end
end
