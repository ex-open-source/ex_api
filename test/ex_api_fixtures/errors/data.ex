import ExApi.{Api, Implementation}

alias ExApi.{Api, Implementation}
alias ExApi.Error.{
  ApiMismatchError,
  ApiNotFoundError,
  ApiNotRegisteredError,
  FeatureNotFoundError,
  ImplementationMismatchError,
  ImplementationNotFoundError,
  ImplementationNotRegisteredError,
  ModuleNotLoadedError,
  NotApiError,
  NotImplementationError,
  WrongApiError
}

defmodule Existing, do: nil

def_api NotFoundApi, do: nil
def_api_impl NotFoundApi, :not_found_impl, do: nil
def_api_impl ConfigApi, :not_found_impl, do: nil

{:ok, config_api} = ExApi.get(ConfigApi)
{:ok, default_impl} = ExApi.get_default_impl(config_api)

api_mismatch = %{config_api | line: 100}
api_not_registered = %Api{module: NotFoundApi}
api_with_existing_module = %Api{module: Existing}
api_with_not_existing_module = %Api{module: NotExisting}
default_group_for_config_api = Application.get_env(:ex_api, :default_groups, %{})[ConfigApi]
feature_that_is_not_found_arity = 0
feature_that_is_not_found_name = :not_found
feature_that_is_not_found = {feature_that_is_not_found_name, feature_that_is_not_found_arity}
impl_existing = %Implementation{
  api: config_api.module,
  module: Existing
}
impl_for_api_that_is_not_found = %Implementation{
  api: NotFoundApi,
  module: NotFoundApi.NotFoundImpl
}
impl_for_wrong_api = %{default_impl | api: AnotherApi}
impl_mismatch = %{default_impl | line: 100}
impl_no_matter = %Implementation{
  api: NoMatter,
  id: :no_matter,
  module: NoMatter
}
impl_not_existing = %Implementation{
  api: config_api.module,
  module: NotExisting,
}
impl_not_found_with_api_registered = ConfigApi.NotFoundImpl.__impl_info__
impl_not_registered = %{default_impl | id: :not_registered}
impl_with_existing_api = %Implementation{
  api: Existing,
  module: NoMatter
}
impl_with_not_existing_api = %Implementation{
  api: NotExisting,
  module: NoMatter
}

[
  # get with api
  {:get, [api: api_mismatch], ApiMismatchError},
  {:get, [api: api_not_registered], ApiNotRegisteredError},
  # get with module
  {:get, [module: api_with_existing_module.module], NotApiError},
  {:get, [module: NotFoundApi], ApiNotFoundError},
  {:get, [module: NotExisting], ModuleNotLoadedError},
  # get_fallback_impl with api
  {:get_fallback_impl, [api: api_mismatch], ApiMismatchError},
  {:get_fallback_impl, [api: api_not_registered], ApiNotRegisteredError},
  # get_fallback_impl with module
  {:get_fallback_impl, [module: api_with_existing_module.module], NotApiError},
  {:get_fallback_impl, [module: NotFoundApi], ApiNotFoundError},
  {:get_fallback_impl, [module: NotExisting], ModuleNotLoadedError},
  # get_default_impl with api
  {:get_default_impl, [api: api_mismatch], ApiMismatchError},
  {:get_default_impl, [api: api_not_registered], ApiNotRegisteredError},
  # get_default_impl with module
  {:get_default_impl, [module: api_with_existing_module.module], NotApiError},
  {:get_default_impl, [module: NotFoundApi], ApiNotFoundError},
  {:get_default_impl, [module: NotExisting], ModuleNotLoadedError},
  # get_feature_support with api and implementation
  {
    :get_feature_support,
    [api: api_mismatch, implementation: impl_no_matter, feature: feature_that_is_not_found],
    ApiMismatchError
  },
  {
    :get_feature_support,
    [api: api_not_registered, implementation: impl_no_matter, feature: feature_that_is_not_found],
    ApiNotRegisteredError
  },
  {
    :get_feature_support,
    [config_api, default_impl, feature_that_is_not_found],
    FeatureNotFoundError,
    [arity: feature_that_is_not_found_arity, implementation: default_impl, name: feature_that_is_not_found_name]
  },
  {
    :get_feature_support,
    [api: config_api, implementation: impl_mismatch, feature: feature_that_is_not_found],
    ImplementationMismatchError
  },
  {
    :get_feature_support,
    [api: config_api, implementation: impl_not_found_with_api_registered, feature: feature_that_is_not_found],
    ImplementationNotRegisteredError
  },
  {
    :get_feature_support,
    [api: config_api, implementation: impl_for_wrong_api, feature: feature_that_is_not_found],
    WrongApiError
  },
  # get_feature_support with api and {group, id}
  {
    :get_feature_support,
    [api_mismatch, {impl_no_matter.group, impl_no_matter.id}, feature_that_is_not_found],
    ApiMismatchError,
    [api: api_mismatch]
  },
  {
    :get_feature_support,
    [api_not_registered, {impl_no_matter.group, impl_no_matter.id}, feature_that_is_not_found],
    ApiNotRegisteredError,
    [api: api_not_registered, id: impl_no_matter.id],
  },
  {
    :get_feature_support,
    [config_api, {default_impl.group, default_impl.id}, feature_that_is_not_found],
    FeatureNotFoundError,
    [arity: feature_that_is_not_found_arity, implementation: default_impl, name: feature_that_is_not_found_name]
  },
  {
    :get_feature_support,
    [
      config_api,
      {impl_not_found_with_api_registered.group, impl_not_found_with_api_registered.id},
      feature_that_is_not_found
    ],
    ImplementationNotFoundError,
    [api: config_api, id: impl_not_found_with_api_registered.id],
  },
  # get_feature_support with module and implementation
  {
    :get_feature_support,
    [module: api_with_existing_module.module, implementation: default_impl, feature: feature_that_is_not_found],
    NotApiError
  },
  {
    :get_feature_support,
    [module: NotFoundApi, implementation: impl_no_matter, feature: feature_that_is_not_found],
    ApiNotFoundError
  },
  {
    :get_feature_support,
    [config_api.module, default_impl, feature_that_is_not_found],
    FeatureNotFoundError,
    [arity: feature_that_is_not_found_arity, implementation: default_impl, name: feature_that_is_not_found_name]
  },
  {
    :get_feature_support,
    [config_api.module, impl_mismatch, feature_that_is_not_found],
    ImplementationMismatchError,
    [api: config_api, implementation: impl_mismatch]
  },
  {
    :get_feature_support,
    [config_api.module, impl_not_registered, feature_that_is_not_found],
    ImplementationNotRegisteredError,
    [api: config_api, implementation: impl_not_registered]
  },
  {
    :get_feature_support,
    [module: NotExisting, implementation: default_impl, feature: feature_that_is_not_found],
    ModuleNotLoadedError
  },
  # get_feature_support with module and {group, id}
  {
    :get_feature_support,
    [api_with_existing_module.module, {impl_no_matter.group, impl_no_matter.id}, feature_that_is_not_found],
    NotApiError,
    [module: api_with_existing_module.module]
  },
  {
    :get_feature_support,
    [NotFoundApi, {impl_no_matter.group, impl_no_matter.id}, feature_that_is_not_found],
    ApiNotFoundError,
    [module: NotFoundApi]
  },
  {
    :get_feature_support,
    [config_api.module, {default_impl.group, default_impl.id}, feature_that_is_not_found],
    FeatureNotFoundError,
    [arity: feature_that_is_not_found_arity, implementation: default_impl, name: feature_that_is_not_found_name]
  },
  {
    :get_feature_support,
    [
      config_api.module,
      {impl_not_found_with_api_registered.group, impl_not_found_with_api_registered.id},
      feature_that_is_not_found
    ],
    ImplementationNotFoundError,
    [api: config_api, id: impl_not_found_with_api_registered.id]
  },
  {
    :get_feature_support,
    [NotExisting, {impl_no_matter.group, impl_no_matter.id}, feature_that_is_not_found],
    ModuleNotLoadedError,
    [module: NotExisting]
  },
  # get_feature_support with impl
  {
    :get_feature_support,
    [impl_for_api_that_is_not_found, feature_that_is_not_found],
    ApiNotFoundError,
    [module: impl_for_api_that_is_not_found.api]
  },
  {
    :get_feature_support,
    [default_impl, feature_that_is_not_found],
    FeatureNotFoundError,
    [arity: feature_that_is_not_found_arity, implementation: default_impl, name: feature_that_is_not_found_name]
  },
  {
    :get_feature_support,
    [impl_not_found_with_api_registered, feature_that_is_not_found],
    ImplementationNotFoundError,
    [api: config_api, id: impl_not_found_with_api_registered.id]
  },
  # get_feature_support with module
  {
    :get_feature_support,
    [impl_for_api_that_is_not_found.module, feature_that_is_not_found],
    ApiNotFoundError,
    [module: impl_for_api_that_is_not_found.api]
  },
  {
    :get_feature_support,
    [default_impl.module, feature_that_is_not_found],
    FeatureNotFoundError,
    [arity: feature_that_is_not_found_arity, implementation: default_impl, name: feature_that_is_not_found_name]
  },
  {
    :get_feature_support,
    [impl_not_found_with_api_registered.module, feature_that_is_not_found],
    ImplementationNotFoundError,
    [api: config_api, id: impl_not_found_with_api_registered.id]
  },
  {
    :get_feature_support,
    [module: NotExisting, feature: feature_that_is_not_found],
    ModuleNotLoadedError
  },
  {:get_feature_support, [module: Existing, feature: feature_that_is_not_found], NotImplementationError},
  # get_impl with api and implementation
  {:get_impl, [api: api_mismatch, implementation: impl_no_matter], ApiMismatchError},
  {:get_impl, [api: api_not_registered, implementation: impl_no_matter], ApiNotRegisteredError},
  {:get_impl, [api: config_api, implementation: impl_mismatch], ImplementationMismatchError},
  {
    :get_impl,
    [api: config_api, implementation: impl_not_found_with_api_registered],
    ImplementationNotRegisteredError
  },
  {:get_impl, [api: config_api, implementation: impl_for_wrong_api], WrongApiError},
  # get_impl with api and {group, id}
  {:get_impl, [api_mismatch, {impl_no_matter.group, impl_no_matter.id}], ApiMismatchError, [api: api_mismatch]},
  {
    :get_impl,
    [api_not_registered, {impl_no_matter.group, impl_no_matter.id}],
    ApiNotRegisteredError,
    [api: api_not_registered, id: impl_no_matter.id],
  },
  {
    :get_impl,
    [config_api, {impl_not_found_with_api_registered.group, impl_not_found_with_api_registered.id}],
    ImplementationNotFoundError,
    [api: config_api, id: impl_not_found_with_api_registered.id],
  },
  # get_impl with module and implementation
  {:get_impl, [module: api_with_existing_module.module, implementation: default_impl], NotApiError},
  {:get_impl, [module: NotFoundApi, implementation: impl_no_matter], ApiNotFoundError},
  {
    :get_impl,
    [config_api.module, impl_mismatch],
    ImplementationMismatchError,
    [api: config_api, implementation: impl_mismatch]
  },
  {
    :get_impl,
    [config_api.module, impl_not_registered],
    ImplementationNotRegisteredError,
    [api: config_api, implementation: impl_not_registered]
  },
  {:get_impl, [module: NotExisting, implementation: default_impl], ModuleNotLoadedError},
  # get_impl with module and {group, id}
  {
    :get_impl,
    [api_with_existing_module.module, {impl_no_matter.group, impl_no_matter.id}],
    NotApiError,
    [module: api_with_existing_module.module]
  },
  {:get_impl, [NotFoundApi, {impl_no_matter.group, impl_no_matter.id}], ApiNotFoundError, [module: NotFoundApi]},
  {
    :get_impl,
    [config_api.module, {impl_not_found_with_api_registered.group, impl_not_found_with_api_registered.id}],
    ImplementationNotFoundError,
    [api: config_api, id: impl_not_found_with_api_registered.id]
  },
  {:get_impl, [NotExisting, {impl_no_matter.group, impl_no_matter.id}], ModuleNotLoadedError, [module: NotExisting]},
  # get_impl with impl
  {:get_impl, [impl_for_api_that_is_not_found], ApiNotFoundError, [module: impl_for_api_that_is_not_found.api]},
  {
    :get_impl,
    [impl_not_found_with_api_registered],
    ImplementationNotFoundError,
    [api: config_api, id: impl_not_found_with_api_registered.id]
  },
  # get_impl with module
  {:get_impl, [impl_for_api_that_is_not_found.module], ApiNotFoundError, [module: impl_for_api_that_is_not_found.api]},
  {
    :get_impl,
    [impl_not_found_with_api_registered.module],
    ImplementationNotFoundError,
    [api: config_api, id: impl_not_found_with_api_registered.id]
  },
  {
    :get_impl,
    [module: NotExisting],
    ModuleNotLoadedError
  },
  {:get_impl, [module: Existing], NotImplementationError},
  # get_impls with api
  {:get_impls, [api: api_mismatch], ApiMismatchError},
  {:get_impls, [api: api_not_registered], ApiNotRegisteredError},
  # get_impls with module
  {:get_impls, [module: api_with_existing_module.module], NotApiError},
  {:get_impls, [module: NotFoundApi], ApiNotFoundError},
  {:get_impls, [module: NotExisting], ModuleNotLoadedError},
  # register_api with api
  {:register_api, [api_with_not_existing_module], ModuleNotLoadedError, [module: api_with_not_existing_module.module]},
  {:register_api, [api_with_existing_module], NotApiError, [module: api_with_existing_module.module]},
  # register_api with module
  {:register_api, [module: api_with_not_existing_module.module], ModuleNotLoadedError},
  {:register_api, [module: api_with_existing_module.module], NotApiError},
  # register_fallback_impl with impl
  {:register_fallback_impl, [impl_for_api_that_is_not_found], ApiNotFoundError, module: impl_for_api_that_is_not_found.api},
  {:register_fallback_impl, [impl_with_not_existing_api], ModuleNotLoadedError, module: impl_with_not_existing_api.api},
  {:register_fallback_impl, [impl_with_existing_api], NotApiError, module: impl_with_existing_api.api},
  {:register_fallback_impl, [impl_not_existing], ModuleNotLoadedError, module: impl_not_existing.module},
  {:register_fallback_impl, [impl_existing], NotImplementationError, [module: impl_existing.module]},
  # register_fallback_impl with module
  {
    :register_fallback_impl,
    [impl_for_api_that_is_not_found.module],
    ApiNotFoundError,
    module: impl_for_api_that_is_not_found.api
  },
  {:register_fallback_impl, [module: impl_not_existing.module], ModuleNotLoadedError},
  {:register_fallback_impl, [module: impl_existing.module], NotImplementationError},
  # register_impl with impl
  {:register_impl, [impl_for_api_that_is_not_found], ApiNotFoundError, module: impl_for_api_that_is_not_found.api},
  {:register_impl, [impl_with_not_existing_api], ModuleNotLoadedError, module: impl_with_not_existing_api.api},
  {:register_impl, [impl_with_existing_api], NotApiError, module: impl_with_existing_api.api},
  {:register_impl, [impl_not_existing], ModuleNotLoadedError, module: impl_not_existing.module},
  {:register_impl, [impl_existing], NotImplementationError, [module: impl_existing.module]},
  # register_impl with module
  {
    :register_impl,
    [impl_for_api_that_is_not_found.module],
    ApiNotFoundError,
    module: impl_for_api_that_is_not_found.api
  },
  {:register_impl, [module: impl_not_existing.module], ModuleNotLoadedError},
  {:register_impl, [module: impl_existing.module], NotImplementationError},
  # set_default_impl with api and impl
  {:set_default_impl, [api: api_mismatch, implementation: impl_no_matter], ApiMismatchError},
  {:set_default_impl, [api: api_not_registered, implementation: impl_no_matter], ApiNotRegisteredError},
  {:set_default_impl, [api: config_api, implementation: impl_mismatch], ImplementationMismatchError},
  {:set_default_impl, [api: config_api, implementation: impl_not_registered], ImplementationNotRegisteredError},
  {:set_default_impl, [api: config_api, implementation: impl_for_wrong_api], WrongApiError},
  # set_default_impl with api and {group, id}
  {
    :set_default_impl,
    [api_mismatch, impl_no_matter.id],
    ApiMismatchError,
    [api: api_mismatch, id: impl_no_matter.id]
  },
  {
    :set_default_impl,
    [api_not_registered, impl_no_matter.id],
    ApiNotRegisteredError,
    [api: api_not_registered, id: impl_no_matter.id]
  },
  {
    :set_default_impl,
    [config_api, {nil, impl_for_api_that_is_not_found.id}],
    ImplementationNotFoundError,
    [api: config_api, group: impl_for_api_that_is_not_found.group, id: impl_for_api_that_is_not_found.id],
  },
  # set_default_impl with module and impl
  {:set_default_impl, [module: NotFoundApi, implementation: impl_no_matter], ApiNotFoundError},
  {
    :set_default_impl,
    [config_api.module, impl_mismatch],
    ImplementationMismatchError,
    [api: config_api, implementation: impl_mismatch]
  },
  {
    :set_default_impl,
    [config_api.module, impl_not_registered],
    ImplementationNotRegisteredError,
    [api: config_api, implementation: impl_not_registered]
  },
  {
    :set_default_impl,
    [module: api_with_not_existing_module.module, implementation: impl_no_matter],
    ModuleNotLoadedError
  },
  {
    :set_default_impl,
    [module: api_with_existing_module.module, implementation: impl_no_matter],
    NotApiError
  },
  {
    :set_default_impl,
    [config_api.module, impl_for_wrong_api],
    WrongApiError,
    [api: config_api, implementation: impl_for_wrong_api]
  },
  # set_default_impl with module and {group, id}
  {
    :set_default_impl,
    [NotFoundApi, impl_no_matter.id],
    ApiNotFoundError,
    [module: NotFoundApi, id: impl_no_matter.id]
  },
  {
    :set_default_impl,
    [config_api.module, impl_for_api_that_is_not_found.id],
    ImplementationNotFoundError,
    [api: config_api, group: default_group_for_config_api, id: impl_for_api_that_is_not_found.id]
  },
  # set_default_impl with impl
  {
    :set_default_impl,
    [impl_for_api_that_is_not_found],
    ApiNotFoundError,
    [module: impl_for_api_that_is_not_found.api]
  },
  {
    :set_default_impl,
    [impl_not_found_with_api_registered],
    ImplementationNotFoundError,
    [api: config_api, group: impl_not_found_with_api_registered.group, id: impl_not_found_with_api_registered.id]
  },
  # set_default_impl with module
  {
    :set_default_impl,
    [impl_for_api_that_is_not_found.module],
    ApiNotFoundError,
    [module: impl_for_api_that_is_not_found.api]
  },
  {
    :set_default_impl,
    [impl_not_found_with_api_registered.module],
    ImplementationNotFoundError,
    [api: config_api, group: impl_not_found_with_api_registered.group, id: impl_not_found_with_api_registered.id]
  },
  {:set_default_impl, [module: impl_not_existing.module], ModuleNotLoadedError},
  {:set_default_impl, [module: impl_existing.module], NotImplementationError},
  # unregister_api with api
  {:unregister_api, [api: api_mismatch], ApiMismatchError},
  {:unregister_api, [api: api_not_registered], ApiNotRegisteredError},
  # unregister_api with module
  {:unregister_api, [module: NotFoundApi], ApiNotFoundError},
  {:unregister_api, [module: api_with_not_existing_module.module], ModuleNotLoadedError},
  {:unregister_api, [module: api_with_existing_module.module], NotApiError},
  # unregister_fallback_impl with api
  {:unregister_fallback_impl, [api: api_mismatch], ApiMismatchError},
  {:unregister_fallback_impl, [api: api_not_registered], ApiNotRegisteredError},
  # unregister_fallback_impl with module
  {:unregister_fallback_impl, [module: NotFoundApi], ApiNotFoundError},
  {:unregister_fallback_impl, [module: api_with_not_existing_module.module], ModuleNotLoadedError},
  {:unregister_fallback_impl, [module: api_with_existing_module.module], NotApiError},
  # unregister_impl with impl
  {
    :unregister_impl,
    [impl_for_api_that_is_not_found],
    ApiNotFoundError,
    module: impl_for_api_that_is_not_found.api
  },
  {
    :unregister_impl,
    [impl_not_registered],
    ImplementationNotFoundError,
    [api: config_api, group: impl_not_registered.group, id: impl_not_registered.id]
  },
  # unregister_impl with id
  {
    :unregister_impl,
    [impl_for_api_that_is_not_found.module],
    ApiNotFoundError,
    module: impl_for_api_that_is_not_found.api
  },
  {
    :unregister_impl,
    [impl_not_found_with_api_registered.module],
    ImplementationNotFoundError,
    [api: config_api, group: impl_not_found_with_api_registered.group, id: impl_not_found_with_api_registered.id]
  },
]
