import ExApi.{Api, Implementation}

def_api ConfigApi do
  @moduledoc """
  This is ConfigApi documentation.
  """

  @doc """
  Another feature ... that is great! It do something.
  """
  @spec another_feature_name() :: String.t
  def_feature another_feature_name()

  @doc """
  An example feature ... It do something.
  """
  @spec feature_name(String.t) :: result :: String.t | {:ok, nil}
  def_feature feature_name(string)

  # no doc here
  @spec yet_another_feature_name(arg1, arg2)
    :: {arg1, arg2} when arg1: atom, arg2: integer
  def_feature yet_another_feature_name(arg1, arg2)

  def_impl :first_impl do
    @moduledoc "Implementation from first source that is beta release and therefore does not have some features like similar services."

    @impl_group :sample

    def_feature_impl another_feature_name, do: "Example result ..."

    def_no_support :feature_name, 1

    def_no_support :yet_another_feature_name, 2
  end
end

def_api_impl ConfigApi, :second_impl do
  # empty implementation
end

def_api_impl ConfigApi, :third_impl, ConfigApi.DifferentModuleName do
  @moduledoc """
  Two lines of ...

  doc.
  """

  @impl_group :sample

  def_no_support :another_feature_name, 0

  # as you can see here is example feature implementation with guard
  def_feature_impl feature_name(string) when is_bitstring(string) do
    "B " <> string
  end
  def_feature_impl feature_name(data) do
    "B " <> to_string(data)
  end

  def_feature_impl yet_another_feature_name(arg1, arg2),
                   do: to_string(arg1) <> to_string(arg2)
end

def_api EmptyConfigApi do
  # empty api without features, documentation and implementations
end

# just define as fast as possible without docs or even set default implementation
def_api NoDefaultImplConfigApi do
  def_feature feature_name(string)

  def_impl :first_impl do
    def_feature_impl feature_name(string), do: string
  end
end

def_api AdditionalApi do
  @spec another_feature_name :: String.t | atom | integer
  def_feature another_feature_name()

  @spec feature_name :: String.t | atom | integer | float
  def_feature feature_name()
end
