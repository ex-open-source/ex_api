alias ExApi.{Api, Feature, Implementation}

%Api{
  default_implementation: {nil, nil},
  doc: "",
  features: %{
    {:feature_name, 1} => %Feature{
      arity: 1,
      doc: "",
      line: 71,
      name: :feature_name,
      signature: {:feature_name, [], [{:string, [line: 71], nil}]},
      spec: []
    }
  },
  implementations: %{
    nil: %{
      first_impl: %Implementation{
        api: NoDefaultImplConfigApi,
        doc: "",
        features: %{
          {:feature_name, 1} => :supported
        },
        id: :first_impl,
        line: 73,
        module: NoDefaultImplConfigApi.FirstImpl
      }
    }
  },
  line: 70,
  module: NoDefaultImplConfigApi,
  pid: GenServer.whereis(NoDefaultImplConfigApi)
}
