alias ConfigApi.FirstImpl
alias ExApi.{Api, Feature, Implementation, Spec}
alias ExApi.Error.{FeatureNotImplementedError, FeatureNotSupportedError}

%Api{
  default_implementation: {:sample, :first_impl},
  doc: "This is ConfigApi documentation.\n",
  features: %{
    {:another_feature_name, 0} => %Feature{
      arity: 0,
      doc: "Another feature ... that is great! It do something.\n",
      line: 12,
      name: :another_feature_name,
      signature: {:another_feature_name, [], []},
      spec: [
        %Spec{
          args: [],
          result: {:|, [line: 11], [
            {:error, {{:., [], [FeatureNotImplementedError, :t]}, [], []}},
            {:|, [line: 11], [
              error: {{:., [], [FeatureNotSupportedError, :t]}, [], []},
              ok: {{:., [line: 11], [
                {:__aliases__, [counter: 0, line: 11], [:String]},
                :t
              ]}, [line: 11], []}
            ]}
          ]},
          when: nil
        }
      ]
    },
    {:feature_name, 1} => %Feature{
      arity: 1,
      doc: "An example feature ... It do something.\n",
      line: 18,
      name: :feature_name,
      signature: {:feature_name, [], [{:string, [line: 18], nil}]},
      spec: [
        %Spec{
          args: [
            {{:., [line: 17], [
              {:__aliases__, [counter: 0, line: 17], [:String]},
              :t
            ]}, [line: 17], []}
          ],
          result: {:|, [line: 17], [
            {:error, {{:., [], [FeatureNotImplementedError, :t]}, [], []}},
            {:|, [line: 17], [
              {:error, {{:., [], [FeatureNotSupportedError, :t]}, [], []}},
              {:::, [line: 17], [
                {:result, [line: 17], nil},
                {:|, [line: 17], [
                  ok: {{:., [line: 17], [
                    {:__aliases__, [counter: 0, line: 17], [:String]},
                    :t
                  ]}, [line: 17], []},
                  ok: nil
                ]}
              ]}
            ]}
          ]},
          when: nil
        }
      ]
    },
    {:yet_another_feature_name, 2} => %Feature{
      arity: 2,
      doc: "",
      line: 23,
      name: :yet_another_feature_name,
      signature: {
        :yet_another_feature_name,
        [],
        [{:arg1, [line: 23], nil}, {:arg2, [line: 23], nil}]
      },
      spec: [
        %Spec{
          args: [
            {:arg1, [line: 21], nil},
            {:arg2, [line: 21], nil}
          ],
          result: {:|, [line: 22], [
            {:error, {{:., [], [FeatureNotImplementedError, :t]}, [], []}},
            {:|, [line: 22], [
              error: {{:., [], [FeatureNotSupportedError, :t]}, [], []},
              ok: {
                {:arg1, [line: 22], nil},
                {:arg2, [line: 22], nil}
              }
            ]}
          ]},
          when: [
            arg1: {:atom, [line: 22], nil},
            arg2: {:integer, [line: 22], nil}
          ]
        }
      ]
    }
  },
  implementations: %{
    nil: %{
      second_impl: %Implementation{
        api: ConfigApi,
        doc: "",
        features: %{
          {:another_feature_name, 0} => :not_implemented,
          {:feature_name, 1} => :not_implemented,
          {:yet_another_feature_name, 2} => :not_implemented,
        },
        id: :second_impl,
        line: 38,
        module: ConfigApi.SecondImpl
      },
    },
    sample: %{
      first_impl: %Implementation{
        api: ConfigApi,
        doc: "Implementation from first source that is beta release and therefore does not have some features like similar services.",
        features: %{
          {:another_feature_name, 0} => :supported,
          {:feature_name, 1} => :not_supported,
          {:yet_another_feature_name, 2} => :not_supported,
        },
        group: :sample,
        id: :first_impl,
        line: 25,
        module: ConfigApi.FirstImpl
      },
      third_impl: %Implementation{
        api: ConfigApi,
        doc: "Two lines of ...\n\ndoc.\n",
        features: %{
          {:another_feature_name, 0} => :not_supported,
          {:feature_name, 1} => :supported,
          {:yet_another_feature_name, 2} => :supported,
        },
        group: :sample,
        id: :third_impl,
        line: 42,
        module: ConfigApi.DifferentModuleName
      }
    }
  },
  line: 3,
  module: ConfigApi,
  pid: GenServer.whereis(ConfigApi)
}
