[
  {ConfigApi, %{nil: 1, sample: 2}, :sample, :first_impl},
  {EmptyConfigApi, %{nil: 0}, nil, nil},
  {NoDefaultImplConfigApi, %{nil: 1}, nil, nil},
]
