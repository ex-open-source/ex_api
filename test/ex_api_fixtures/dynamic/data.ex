alias DynamicApi.{FirstDynamicImpl, SecondDynamicImpl}
[
  {:register_api, :register_api, [DynamicApi]},
  {:register_api_again, :register_api, [DynamicApi]},
  {:register_first_impl, :register_impl, [FirstDynamicImpl]},
  {:register_second_impl, :register_impl, [SecondDynamicImpl, :overwritten_group_name]},
  {
    :set_default_first_impl,
    :set_default_impl,
    [DynamicApi, {nil, :first_dynamic_impl}]
  },
  {:set_default_second_impl, :set_default_impl, [DynamicApi, {:overwritten_group_name, :second_dynamic_impl}]},
  {:unregister_second_impl, :unregister_impl, [SecondDynamicImpl, :overwritten_group_name]},
  {:unregister_first_impl, :unregister_impl, [FirstDynamicImpl]},
]
