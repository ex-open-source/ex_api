alias DynamicApi.SecondDynamicImpl
alias ExApi.Event.UnregisteredEvent
alias ExApi.Implementation

%UnregisteredEvent{
  data: %Implementation{
    api: DynamicApi,
    doc: "",
    features: %{},
    group: :overwritten_group_name,
    id: :second_dynamic_impl,
    line: 7,
    module: SecondDynamicImpl
  }
}
