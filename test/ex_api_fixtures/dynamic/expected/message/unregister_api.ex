alias ExApi.Api
alias ExApi.Event.UnregisteredEvent

%UnregisteredEvent{
  data: %Api{
    default_implementation: {:overwritten_group_name, :second_dynamic_impl},
    doc: "",
    features: %{},
    implementations: %{nil: %{}, overwritten_group_name: %{}},
    line: 3,
    module: DynamicApi
  }
}
