alias DynamicApi.{FirstDynamicImpl, SecondDynamicImpl}
alias ExApi.{Api, Implementation}

%ExApi.Event.ChangedDefaultImplementationEvent{
  api: %Api{
    default_implementation: {:overwritten_group_name, :second_dynamic_impl},
    doc: "",
    features: %{},
    implementations: %{
      nil: %{
        first_dynamic_impl: %Implementation{
          api: DynamicApi,
          doc: "",
          features: %{},
          id: :first_dynamic_impl,
          line: 4,
          module: FirstDynamicImpl
        }
      },
      overwritten_group_name: %{
        second_dynamic_impl: %Implementation{
          api: DynamicApi,
          doc: "",
          features: %{},
          group: :overwritten_group_name,
          id: :second_dynamic_impl,
          line: 7,
          module: SecondDynamicImpl
        }
      }
    },
    line: 3,
    module: DynamicApi
  },
  implementation: %Implementation{
    api: DynamicApi,
    doc: "",
    features: %{},
    group: :overwritten_group_name,
    id: :second_dynamic_impl,
    line: 7,
    module: SecondDynamicImpl
  }
}
