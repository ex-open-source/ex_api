alias DynamicApi.FirstDynamicImpl
alias ExApi.Event.UnregisteredEvent
alias ExApi.Implementation

%UnregisteredEvent{
  data: %Implementation{
    api: DynamicApi,
    doc: "",
    features: %{},
    id: :first_dynamic_impl,
    line: 4,
    module: FirstDynamicImpl
  }
}
