alias DynamicApi.FirstDynamicImpl
alias ExApi.Event.RegisteredEvent
alias ExApi.Implementation

%RegisteredEvent{
  again: false,
  data: %Implementation{
    api: DynamicApi,
    doc: "",
    features: %{},
    id: :first_dynamic_impl,
    line: 4,
    module: FirstDynamicImpl
  }
}
