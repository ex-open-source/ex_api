alias DynamicApi.FirstDynamicImpl
alias ExApi.{Api, Implementation}

%Api{
  default_implementation: {nil, nil},
  doc: "",
  features: %{},
  implementations: %{
    nil: %{
      first_dynamic_impl: %Implementation{
        api: DynamicApi,
        doc: "",
        features: %{},
        id: :first_dynamic_impl,
        line: 4,
        module: FirstDynamicImpl
      }
    }
  },
  line: 3,
  module: DynamicApi
}
