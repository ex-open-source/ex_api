alias DynamicApi.FirstDynamicImpl
alias ExApi.{Api, Implementation}

%Api{
  default_implementation: {:overwritten_group_name, :second_dynamic_impl},
  doc: "",
  features: %{},
  implementations: %{
    nil: %{
      first_dynamic_impl: %Implementation{
        api: DynamicApi,
        doc: "",
        features: %{},
        id: :first_dynamic_impl,
        line: 4,
        module: FirstDynamicImpl
      }
    },
    overwritten_group_name: %{}
  },
  line: 3,
  module: DynamicApi
}
