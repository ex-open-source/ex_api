import ExApi.{Api, Implementation}

def_api DynamicApi do
  def_impl :first_dynamic_impl, do: nil
end

def_api_impl DynamicApi, :second_dynamic_impl, do: nil
