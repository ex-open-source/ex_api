defmodule ExApi.Api do
  @moduledoc """
  Defines struct for api and macro to create it.

      import ExApi.Api
      def_api MyApi do
        # api contents here ...
      end
  """

  alias ExApi.{Feature, Implementation}

  @enforce_keys [:module]

  defstruct [
    :default_implementation,
    :fallback_implementation,
    :line,
    :module,
    :pid,
    doc: "",
    features: %{},
    implementations: []
  ]

  @type t :: %__MODULE__{
    default_implementation: {group_name :: atom, id :: atom},
    doc: String.t,
    fallback_implementation: Implementation.t,
    features: %{
      optional({funtion_name :: atom, arity :: non_neg_integer}) => Feature.t
    },
    implementations: %{
      optional(group_name :: atom) => list(Implementation.t)
    },
    line: non_neg_integer,
    module: module,
    pid: pid,
  }

  @doc """
  Defines an api. An api definas features that are optional to implement.
  You can preview on api extra doc pages which all documented features.

  ## Attributes

  1. `@moduledoc` - defines documentation for api.
  2. `@doc` - defines documentation for feature
  3. `@spec` - defines spec for feature

  Example:

      def_api MyLibrary.Api do
        @moduledoc "Describe why you created this api and features that you expect to implementation."

        @doc "Describe what this feature will do here ..."
        @spec feature_name(String.t) :: String.t
        def_feature feature_name(string)
      end

  """
  @spec def_api(module, opts :: Keyword.t) :: {:module, module, binary, term}
  defmacro def_api(name, do: block) do
    module_line = __CALLER__.line
    quote do
      defmodule unquote(name) do
        use GenServer

        alias ExApi.{Api, Feature, Spec}
        alias ExApi.Error.ImplementationNotFoundError

        import ExApi.Kernel, only: [def_feature: 1, def_impl: 2]
        import Kernel, except: [
          def: 1, def: 2, defmacro: 1, defmacro: 2,
          defmacrop: 1, defmacrop: 2, defp: 1, defp: 2,
        ]

        require Implementation

        @info %{
          doc: "",
          line: unquote(module_line),
          features: %{}
        }

        Module.register_attribute(
          __MODULE__,
          :do_after_compile,
          accumulate: true
        )

        @after_compile __MODULE__

        def __after_compile__(_env, _bytecode) do
          data = Module.delete_attribute(__MODULE__, :do_after_compile)
          for {id, opts, line} <- data do
            Implementation.def_api_impl(__MODULE__, id, nil, line, opts)
          end
        end

        _ = unquote(block)

        @info %{@info | doc: @moduledoc || ""}

        Spec.add_spec(__MODULE__)

        @doc "Starts Api GenServer server."
        @spec start_link :: GenServer.on_start
        def start_link, do: GenServer.start_link(__MODULE__, [], name: __MODULE__)

        @doc "Callback implementation of: `c:GenServer.init/1`."
        @spec init(args :: term) ::
          {:ok, state} |
          {:ok, state, timeout | :hibernate} |
          :ignore |
          {:stop, reason :: any}
          when state: any
        def init(state), do: {:ok, state}

        unquote(feature_for_code())

        @doc "Returns helpful informations about this api like docs and specs."
        @spec __api_info__ :: %{
          required(:doc) => String.t,
          required(:features) => %{
            optional({funtion_name :: atom, arity :: non_neg_integer})
              => Feature.t
          }
        }
        Kernel.def __api_info__, do: @info

        @doc "Shorter version for `ExApi.get_default_impl/1`."
        @spec get_default_impl :: {:ok, Implementation.t | nil}
        Kernel.def get_default_impl, do: ExApi.get_default_impl(__MODULE__)

        @doc "Shorter version for `ExApi.get_impl/2`."
        @spec get_impl(Implementation.t) :: {:ok, Implementation.t}
              | {:error, ImplementationMismatchError.t}
              | {:error, ImplementationNotRegisteredError.t}
              | {:error, WrongApiError.t}
        @spec get_impl(atom) :: {:ok, Implementation.t}
              | {:error, ImplementationNotFoundError.t}
        Kernel.def get_impl(impl_or_id),
                   do: ExApi.get_impl(__MODULE__, impl_or_id)

        @doc "Shorter version for `ExApi.get_impls/1`."
        @spec get_impls :: {:ok, list(Implementation.t)}
        Kernel.def get_impls, do: ExApi.get_impls(__MODULE__)

        @doc "Shorter version for `ExApi.set_default_impl/1`."
        @spec set_default_impl(Implementation.t | atom) :: :ok
        Kernel.def set_default_impl(impl_or_id),
                   do: ExApi.set_default_impl(__MODULE__, impl_or_id)
      end
    end
  end

  defp feature_for_code do
    quote unquote: false do
      if Enum.count(@info.features) > 0 do
        @doc "Calls a feature on specified implementation."
        @spec feature_for(Implementation.t | {atom, atom}, atom, list(term), function, function)
              :: {:ok, pid}
              | {:error, ImplementationMismatchError.t}
              | {:error, ImplementationNotFoundError.t}
              | {:error, ImplementationNotRegisteredError.t}
              | {:error, WrongApiError.t}
        def feature_for(impl_or_group_with_id, name, args, success_callback, error_callback \\ fn _ -> nil end) do
          case get_impl(impl_or_group_with_id) do
            {:ok, impl} ->
              request = {:feature_for, impl, name, args, success_callback, error_callback}
              GenServer.cast(__MODULE__, request)
              {:ok, GenServer.whereis(__MODULE__)}
            result = {:error, _error} -> result
          end
        end
        def handle_cast({:feature_for, impl, name, args, success_callback, error_callback}, state) do
          case apply(impl.module, name, args) do
            {:ok, result} -> apply(success_callback, [result])
            {:error, error} -> apply(error_callback, [error])
          end
          {:noreply, state}
        end
      end
    end
  end
end
