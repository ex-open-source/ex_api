defmodule ExApi.Kernel do
  @moduledoc """
  Defines macros that will be imported automatically and used by you in api and api implementation body.
  """

  alias ExApi.{Error, Feature, Spec}
  alias ExApi.Error.FeatureNotSupportedError

  @doc "Defines feature."
  @spec def_feature(call :: Macro.expr) :: Macro.expr
  defmacro def_feature({name, [line: line], args}) do
    escaped_args = Macro.escape(args)
    signature = Macro.escape({name, [], args})
    quote do
      args = unquote(escaped_args)
      arity = Enum.count(args)
      name = unquote(name)
      feature_doc = case Module.delete_attribute(__MODULE__, :doc) do
        {_line, doc} -> doc
        doc -> doc
      end
      data = %Feature{
        arity: arity,
        doc: feature_doc || "",
        line: unquote(line),
        name: name,
        signature: unquote(signature),
        spec: []
      }
      Feature.create(__MODULE__, name, arity, data)
    end
  end

  @doc "Defines feature implementation."
  @spec def_feature_impl(call :: Macro.expr, opts :: Macro.expr) :: Macro.expr
  defmacro def_feature_impl(call, do: content) do
    {name, args} = Spec.get_call_info(call)
    key = {name, Enum.count(args)}
    quote do
      key = unquote(key)
      if Feature.is_api_feature(@info.api, key) do
        Feature.set_status(__MODULE__, key, :supported)
        @doc @info.api.__api_info__.features[key].doc
        specs = Spec.supported_spec(@info.api.__api_info__.features[key])
        for spec <- specs do
          Module.put_attribute(__MODULE__, :spec, Spec.with_ref(spec))
        end
        def unquote(call) do
          case unquote(content) do
            result = {:ok, _result} -> result
            result = {:error, _error} -> result
            result -> {:ok, result}
          end
        end
      end
    end
  end

  @doc "Shorter version for `ExApi.Api.def_api_impl/3`."
  @spec def_impl(id :: atom, [do: Macro.expr])
    :: {:module, impl_name :: module, binary, term}
  defmacro def_impl(id, opts) when is_atom(id) do
    line = __CALLER__.line
    quote do
      @do_after_compile {
        unquote(id),
        unquote(Macro.escape(opts)),
        unquote(line)
      }
    end
  end

  @doc "Defines no support for feature."
  @spec def_no_support(name :: atom, arity :: non_neg_integer) :: Macro.expr
  defmacro def_no_support(name, arity) do
    {^name, ^arity, args} = Feature.add_args({name, arity})
    key = {name, arity}
    quote do
      key = unquote(key)
      if Feature.is_api_feature(@info.api, key) do
        Feature.set_status(__MODULE__, key, :not_supported)
        @doc "This feature is not supported."
        specs = Spec.not_supported_spec(@info.api.__api_info__.features[key])
        for spec <- specs do
          Module.put_attribute(__MODULE__, :spec, Spec.with_ref(spec))
        end
        def unquote(name)(unquote_splicing(args)) do
          {:ok, api} = ExApi.get(@info.api)
          {:ok, impl} = ExApi.get_impl(__MODULE__)
          data = [
            api: api,
            feature: api.features[unquote(key)],
            implementation: impl,
          ]
          {:error, Error.prepare(FeatureNotSupportedError, data)}
        end
      end
    end
  end
end
