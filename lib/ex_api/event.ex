defmodule ExApi.Event do
  @moduledoc false

  defmodule ChangedDefaultImplementationEvent do
    @moduledoc "This event is send when you change default api implementation."

    alias ExApi.{Api, Implementation}

    defstruct [:api, :implementation]

    @type t :: %__MODULE__{api: Api.t, implementation: Implementation.t}
  end

  defmodule RegisteredEvent do
    @moduledoc "This event is send when you register api or its implementation."

    alias ExApi.{Api, Implementation}

    defstruct [:again, :data, fallback: false]

    @type t :: %__MODULE__{
      again: boolean,
      data: Api.t | Implementation.t,
      fallback: boolean
    }
  end

  defmodule UnregisteredEvent do
    @moduledoc "This event is send when you unregister api or its implementation."

    alias ExApi.{Api, Implementation}

    defstruct [:data, fallback: false]

    @type t :: %__MODULE__{data: Api.t | Implementation.t, fallback: true}
  end
end
