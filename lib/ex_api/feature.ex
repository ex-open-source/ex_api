defmodule ExApi.Feature do
  @moduledoc """
  Defines struct for storing feature data and helpful methods used internally.
  """

  fields = [:arity, :doc, :line, :name, :signature, :spec]

  @enforce_keys fields

  defstruct fields

  @type t :: %__MODULE__{
    arity: non_neg_integer,
    doc: String.t,
    line: non_neg_integer,
    name: atom,
    signature: Macro.expr,
    spec: Macro.expr
  }

  @doc false
  def add_args({name, arity = 0}), do: {name, arity, []}
  def add_args({name, arity}) do
    args = for index <- 1..arity do
      "_var" <> Integer.to_string(index)
      |> String.to_atom
      |> Macro.var(nil)
    end
    {name, arity, args}
  end

  @doc false
  def create(module, name, arity, feature) do
    key = {name, arity}
    updated_features =
      module
      |> get()
      |> put_in([key], feature)
    put(module, updated_features)
  end

  @doc false
  def get(module) do
    module
    |> Module.get_attribute(:info)
    |> Map.fetch!(:features)
  end

  @doc false
  def is_api_feature(api, key) do
    features = api.__api_info__.features
    Map.has_key?(features, key)
  end

  @doc false
  def missing(impl_info) do
    all_features = Map.keys(impl_info.api.__api_info__.features)
    added_features = Map.keys(impl_info.features)
    Enum.map(all_features -- added_features, &add_args/1)
  end

  @doc false
  def put(module, value) do
    info_value =
      module
      |> Module.get_attribute(:info)
      |> put_in([Access.key!(:features)], value)
    Module.put_attribute(module, :info, info_value)
  end

  @doc false
  def set_status(module, key, status) do
    updated_features =
      module
      |> get()
      |> put_in([key], status)
    put(module, updated_features)
  end
end
