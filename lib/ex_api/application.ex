defmodule ExApi.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    pub_sub = supervisor(Phoenix.PubSub.PG2, [:ex_api, []])
    children = if Mix.env != :test, do: [pub_sub, ExApi], else: [pub_sub]
    opts = [strategy: :one_for_one, name: ExApi.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
