defmodule ExApi.Implementation do
  @moduledoc """
  Defines struct for api implementation and macro to create it.

      import ExApi.Implementation
      def_api_impl MyLibrary.Api, :impl_id do
        # api implementation contents here ...
      end
  """

  alias ExApi.{Error, Spec}
  alias ExApi.Error.FeatureNotImplementedError

  @enforce_keys [:api, :module]

  defstruct [:api, :group, :id, :line, :module, doc: "", features: []]

  @type t :: %__MODULE__{
    api: module,
    doc: String.t,
    features: %{
      optional({funtion_name :: atom, arity :: non_neg_integer})
        => :suported | :not_supported | :not_implemented
    },
    group: atom,
    id: atom,
    line: non_neg_integer,
    module: module
  }

  @doc """
  Defines an api implementation. api implementation implements as much features as possible.
  You can preview on api implementation extra doc pages which features are supported, not supported or simply not implemented yet.

  **Note**: Don't set manually `line` argument. It's used internally when defining implementation inside api.

  ## Attributes

  1. `@moduledoc` - defines documentation for api implementation.
  2. `@impl_group` - name of implementations group (default: `nil`)

  Example:

      def_api_impl MyApi, :my_id do
        @moduledoc \"""
        Describe what you are implementing for this api.
        For example you can implement api to use specific 3rd-party service.
        Also you can describe a configuration that will be used by your implementation.
        In this case you can for example allow to optionally change remote host and/or port.
        \"""

        def_feature_impl feature_name(data), do: to_string(data)

        def_no_support :another_feature_name, 1
      end

  ## You can define api implementation in three ways:

  1. By specifing only id of api implementation.

  ```elixir
  def_api_impl MyApi, :my_id do
    # ...
  end
  ```

  2. By specifing id and module of api implementation, like:

  ```elixir
  def_api_impl MyApi, :my_id, MyApi.MyId do
    # ...
  end
  ```

  3. Inside api:

  ```elixir
  def_api MyApi do
    # api contents here ...

    def_impl :my_id do
      # implementation content goes here ...
    end
  end
  ```

  All examples above defines same implementation with module name `MyApi.MyId` and id `:my_id` for `MyApi` api.
  """
  @spec def_api_impl(module, atom, module, non_neg_integer, Keyword.t)
        :: {:module, module, binary, term}
  defmacro def_api_impl(api, id, name \\ nil, line \\ nil, opts) do
    function_generator = quote unquote: false do
      for {name, arity, args} <- Feature.missing(@info) do
        key = {name, arity}
        Feature.set_status(__MODULE__, key, :not_implemented)
        @doc "This feature is not implemented yet."
        specs = Spec.not_implemented_spec(@info.api.__api_info__.features[key])
        for spec <- specs do
          Module.put_attribute(__MODULE__, :spec, Spec.with_ref(spec))
        end
        def unquote(name)(unquote_splicing(args)) do
          {:ok, api} = ExApi.get(@info.api)
          {:ok, impl} = ExApi.get_impl(__MODULE__)
          data = [
            api: api,
            feature: api.features[unquote(key)],
            implementation: impl
          ]
          {:error, Error.prepare(FeatureNotImplementedError, data)}
        end
      end
    end
    quote do
      api = unquote(api)
      line = unquote(line)
      module_line = if line, do: line, else: unquote(__CALLER__.line)
      defmodule Spec.prepae_impl_name(unquote(name), api, unquote(id)) do
        import ExApi.Kernel, only: [def_feature_impl: 2, def_no_support: 2]
        import Kernel, except: [
          def: 1, def: 2, defmacro: 1, defmacro: 2,
          defmacrop: 1, defmacrop: 2, defp: 1, defp: 2,
        ]

        alias ExApi.{Feature, Implementation, Spec}

        require ExApi.Spec

        @impl_group Application.get_env(:ex_api, :default_groups, %{})[api]

        @info %Implementation{
          api: api,
          doc: @moduledoc || "",
          features: %{},
          id: unquote(id),
          line: module_line,
          module: __MODULE__
        }

        case unquote(opts) do
          [do: block] ->
            Code.eval_quoted(block, [], __ENV__)
          _ -> nil
        end

        @info %{@info | doc: @moduledoc || "", group: @impl_group}

        unquote(function_generator)

        @doc "Returns helpful informations about this implementation like docs and specs."
        @spec __impl_info__ :: Implementation.t
        Kernel.def __impl_info__, do: @info
      end
    end
  end
end
