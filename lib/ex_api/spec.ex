defmodule ExApi.Spec do
  @moduledoc """
  Defines struct for storing specification parts and helpful methods used internally.
  """

  alias ExApi.Error.{FeatureNotImplementedError, FeatureNotSupportedError}
  alias ExApi.Feature

  fields = [:args, :result, :when]

  @enforce_keys fields

  defstruct fields

  @type t :: %__MODULE__{
    args: Macro.expr,
    result: Macro.expr,
    when: Macro.expr
  }

  @doc false
  def add_spec(module) do
    specs =
      module
      |> Module.delete_attribute(:spec)
      |> Enum.map(&edit_spec/1)
    for {name, spec} <- specs do
      key = {name, Enum.count(spec.args || [])}
      path = [key, Access.key!(:spec)]
      updated_features =
        module
        |> Feature.get()
        |> update_in(path, &([spec | &1]))
      Feature.put(module, updated_features)
    end
  end

  defp edit_spec({:spec, ast, _}) do
    {args, data, name, result, when_keyword} = case ast do
      {:::, data, [{name, _, args}, result]} ->
        {args, data, name, result, nil}
      {:when, data, [{:::, _, [{name, _, args}, result]}, when_keyword]} ->
        {args, data, name, result, when_keyword}
    end
    {name, %__MODULE__{
      args: args,
      result: {:|, data, [
        {:error, {{:., [], [FeatureNotImplementedError, :t]}, [], []}},
        {:|, data, [
          {:error, {{:., [], [FeatureNotSupportedError, :t]}, [], []}},
          parse_spec(result)
        ]}
      ]},
      when: when_keyword
    }}
  end

  defp parse_spec({:|, data, [first, second = {:|, _, _}]}), do:
       {:|, data, [do_parse_spec(first, :ok), do_parse_spec(second)]}
  defp parse_spec({:|, data, [first, second]}), do:
       {:|, data, [do_parse_spec(first, :ok), do_parse_spec(second, :error)]}
  defp parse_spec({:::, data, [first, second]}), do:
       {:::, data, [first, parse_spec(second)]}
  defp parse_spec(spec), do: do_parse_spec(spec, :ok)

  defp do_parse_spec({:|, data, [first, second = {:|, _, _}]}), do:
    {:|, data, [do_parse_spec(first, :error) | do_parse_spec(second, :error)]}
  defp do_parse_spec({:|, data, [first, second]}), do:
    {:|, data, [do_parse_spec(first, :error), do_parse_spec(second, :error)]}
  defp do_parse_spec({:ok, ast}, _default), do: {:ok, ast}
  defp do_parse_spec({:error, ast}, _default), do: {:error, ast}
  defp do_parse_spec(ast, default), do: {default, ast}

  @doc false
  def get_call_info({:when, _when_info, [{name, _func_info, args}, _checks]}),
      do: {name, args}
  def get_call_info({name, _func_info, nil}), do: {name, []}
  def get_call_info({name, _func_info, args}), do: {name, args}

  @doc false
  def not_implemented_spec(feature),
      do: spec_with_error_in_return(feature, FeatureNotImplementedError)

  @doc false
  def not_supported_spec(feature),
      do: spec_with_error_in_return(feature, FeatureNotSupportedError)

  defp spec_with_error_in_return(feature, error) do
    error = {:error, {{:., [], [error, :t]}, [], []}}
    for spec <- feature.spec do
      if is_nil(spec.when) do
        {:::, [], [{feature.name, [], spec.args}, error]}
      else
        {:when, [], [
          {:::, [], [{feature.name, [], spec.args}, error]},
          spec.when
        ]}
      end
    end
  end

  @doc false
  def prepae_impl_name(nil, api, id) do
    camelized_id =
      id
      |> Atom.to_string
      |> Macro.camelize
    String.to_atom("#{api}.#{camelized_id}")
  end
  def prepae_impl_name(name, _api, _id), do: name

  @doc false
  def supported_spec(feature) do
    for spec <- feature.spec do
      result = skip_errors(spec.result)
      if is_nil(spec.when) do
        {:::, [], [{feature.name, [], spec.args}, result]}
      else
        {:when, [], [
          {:::, [], [{feature.name, [], spec.args}, result]},
          spec.when
        ]}
      end
    end
  end

  defp skip_errors({:|, _, [_, {:|, _, [_, spec]}]}), do: spec

  @doc false
  defmacro with_ref(spec) do
    quote do
      reference = if @spec == [] do
        @spec sample :: :ok
        [{:spec, _, {ref, _}}] = @spec
        Module.delete_attribute(__MODULE__, :spec)
        ref
      else
        {:spec, _, {ref, _}} = List.first(@spec)
        ref
      end
      {:spec, unquote(spec), {reference, {1, 1}}}
    end
  end
end
