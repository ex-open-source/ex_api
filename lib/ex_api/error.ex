defmodule ExApi.Error do
  @moduledoc "Helper module for managing errors."

  defmodule ApiMismatchError do
    @moduledoc "This error is returned/send when you pass `ExApi.Api` struct that does not matches api found in state."

    alias ExApi.Api

    defexception [:api, :message]

    @type t :: %__MODULE__{api: Api.t, message: String.t}

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.api.module)
      "Api `#{api_module}` does not match registered api with same module."
    end
  end

  defmodule ApiNotFoundError do
    @moduledoc "This error is returned/send when you pass module of api that could not be found in state."

    defexception [:message, :module]

    @type t :: %__MODULE__{message: String.t, module: module}

    def message(exception) do
      "Elixir." <> module = Atom.to_string(exception.module)
      "Module `#{module}` is not found in apis state."
    end
  end

  defmodule ApiNotRegisteredError do
    @moduledoc "This error is returned/send when you pass `ExApi.Api` struct that is not yet registered in state."

    alias ExApi.Api

    defexception [:api, :message]

    @type t :: %__MODULE__{api: Api.t, message: String.t}

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.api.module)
      "api `#{api_module}` is not registered."
    end
  end

  defmodule FeatureNotImplementedError do
    @moduledoc "This error is returned when you try to call feature that is not yet implemented by specified implementation."

    alias ExApi.{Api, Feature, Implementation}

    defexception [:api, :feature, :implementation, :message]

    @type t :: %__MODULE__{
      api: Api.t,
      feature: Feature.t,
      implementation: Implementation.t,
      message: String.t
    }

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.api.module)
      "Elixir." <> impl_module = Atom.to_string(exception.implementation.module)
      "`#{impl_module}` does not implements api `#{api_module}`s feature `#{exception.feature.name}/#{exception.feature.arity}`."
    end
  end

  defmodule FeatureNotFoundError do
    @moduledoc "This error is returned when you try to get feature that is not defined in api."

    alias ExApi.Implementation

    defexception [:arity, :implementation, :message, :name]

    @type t :: %__MODULE__{arity: non_neg_integer, implementation: Implementation.t, message: String.t name: atom}

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.implementation.api)
      "Feature `#{api_module} does not defines `#{exception.name}/#{exception.arity}` feature."
    end
  end

  defmodule FeatureNotSupportedError do
    @moduledoc "This error is returned when you try to call feature that is not supported by specified implementation."

    alias ExApi.{Api, Feature, Implementation}

    defexception [:api, :feature, :implementation, :message]

    @type t :: %__MODULE__{
      api: Api.t,
      feature: Feature.t,
      implementation: Implementation.t,
      message: String.t
    }

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.api.module)
      "Elixir." <> impl_module = Atom.to_string(exception.implementation.module)
      "`#{impl_module}` does not support implementation for api `#{api_module}`s feature `#{exception.feature.name}/#{exception.feature.arity}`."
    end
  end

  defmodule ImplementationMismatchError do
    @moduledoc "This error is returned/send when you pass `ExApi.Implementation` struct that does not matches implementation found in state."

    alias ExApi.{Api, Implementation}

    defexception [:api, :implementation, :message]

    @type t :: %__MODULE__{
      api: Api.t,
      implementation: Implementation.t,
      message: String.t
    }

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.api.module)
      "Impl with id `:#{exception.implementation.id}` does not match registered implementation with same id for `#{api_module}` api."
    end
  end

  defmodule ImplementationNotFoundError do
    @moduledoc "This error is returned/send when you pass module of implementation that could not be found in state."

    alias ExApi.Api

    defexception [:api, :group, :id, :message]

    @type t :: %__MODULE__{
      api: Api.t,
      group: atom,
      id: atom,
      message: String.t
    }

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.api.module)
      "Implementation with id `:#{exception.id}` is not found in `#{api_module}` api state."
    end
  end

  defmodule ImplementationNotRegisteredError do
    @moduledoc "This error is returned/send when you pass `ExApi.Implementation` struct that is not yet registered in state."

    alias ExApi.{Api, Implementation}

    defexception [:api, :implementation, :message]

    @type t :: %__MODULE__{
      api: Api.t,
      implementation: Implementation.t,
      message: String.t
    }

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.api.module)
      "Implementation with id `:#{exception.implementation.id}` is not registered for api `#{api_module}`."
    end
  end

  defmodule ModuleNotLoadedError do
    @moduledoc "This error is returned/send when specified module is not yet loaded."

    defexception [:message, :module]

    @type t :: %__MODULE__{message: String.t, module: module}

    def message(exception) do
      "Elixir." <> module = Atom.to_string(exception.module)
      "Module `#{module}` is not yet loaded."
    end
  end

  defmodule NotApiError do
    @moduledoc "This error is returned/send when specified module is already loaded, but it's not api."

    defexception [:message, :module]

    @type t :: %__MODULE__{message: String.t, module: module}

    def message(exception) do
      "Elixir." <> module = Atom.to_string(exception.module)
      "Module `#{module}` is not api."
    end
  end

  defmodule NotImplementationError do
    @moduledoc "This error is returned/send when specified module is already loaded, but it's not implementation."

    defexception [:message, :module]

    @type t :: %__MODULE__{message: String.t, module: module}

    def message(exception) do
      "Elixir." <> module = Atom.to_string(exception.module)
      "Module `#{module}` is not implementation."
    end
  end

  defmodule WrongApiError do
    @moduledoc "This error is returned/send when you pass implementation with wrong api."

    alias ExApi.{Api, Implementation}

    defexception [:api, :implementation, :message]

    @type t :: %__MODULE__{
      api: Api.t,
      implementation: Implementation.t,
      message: String.t
    }

    def message(exception) do
      "Elixir." <> api_module = Atom.to_string(exception.api.module)
      "Elixir." <> impl_module = Atom.to_string(exception.implementation.module)
      "Found implementation `#{impl_module}` for api `#{exception.implementation.api}`, but got `#{api_module}` api."
    end
  end

  @doc "Creates error and calls `prepare/1` function."
  @spec prepare(module, Keyword.t) :: struct
  def prepare(error, data) when is_atom(error) and is_list(data) do
    error_struct = struct(error, data)
    prepare(error_struct)
  end

  @doc "Auto generates error message and adds it into struct."
  @spec prepare(struct) :: struct
  def prepare(error) when is_map(error),
      do: %{error | message: error.__struct__.message(error)}
end
