defmodule ExApi.Cluster do
  @moduledoc "Helper module for working with multiple nodes."

  @doc "Shortcut function for `:rpc.block_call/5`."
  @spec block_call(atom, module, atom, list(term)) :: term | {:badrpc, reason :: term}
  def block_call(node_name, module, function, args, timeout \\ :infinity),
      do: :rpc.block_call(add_host(node_name), module, function, args, timeout)

  @doc "Turn current node into a distributed node, creates other nodes and finally connect all of them."
  def spawn(node_names) do
    :net_kernel.start([:"primary@127.0.0.1"])
    :erl_boot_server.start([])
    :erl_boot_server.add_slave({127, 0, 0, 1})
    node_names
    |> Enum.map(&Task.async(fn -> spawn_node(&1) end))
    |> Enum.each(&Task.await(&1, 30_000))
    [first_node_name | rest_nodes_names] = node_names
    for node_name <- rest_nodes_names, do: block_call(first_node_name, Node, :connect, [add_host(node_name)])
  end

  defp add_host(node_name), do: :"#{node_name}@127.0.0.1"

  defp spawn_node(node_name) do
    loader_args = '-loader inet -hosts 127.0.0.1 -setcookie #{:erlang.get_cookie()}'
    {:ok, _node} = :slave.start(:"127.0.0.1", node_name, loader_args)
    block_call(node_name, :code, :add_paths, [:code.get_path()])
    block_call(node_name, Mix, :env, [Mix.env()])
    for app_name <- [:mix, :phoenix_pubsub, :ex_api],
        do: block_call(node_name, Application, :ensure_all_started, [app_name])
  end
end
