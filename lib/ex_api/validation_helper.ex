defmodule ExApi.ValidationHelper do
  @moduledoc "Helper module for validating passed arguments."

  alias ExApi.{Api, Error, Implementation}

  alias ExApi.Error.{
    ApiMismatchError,
    ApiNotFoundError,
    ApiNotRegisteredError,
    FeatureNotFoundError,
    ImplementationMismatchError,
    ImplementationNotFoundError,
    ImplementationNotRegisteredError,
    ModuleNotLoadedError,
    NotApiError,
    NotImplementationError,
    WrongApiError
  }

  @doc "Checks if passed api or its module is found or loaded and registered. Then matches it with found api in state."
  @spec with_api_check(Api.t, function) :: term
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
  @spec with_api_check(module, function) :: term
        | {:error, ApiNotFoundError.t}
  def with_api_check(api = %Api{}, success_callback)
      when is_function(success_callback) do
    {:ok, state} = ExApi.get_state
    case state[api.module] do
      nil ->
        error = %ApiNotRegisteredError{api: api}
        {:error, Error.prepare(error)}
      ^api ->
        apply(success_callback, [api])
      _ ->
        error = %ApiMismatchError{api: api}
        {:error, Error.prepare(error)}
    end
  end
  def with_api_check(module, success_callback)
      when is_atom(module) and is_function(success_callback) do
    with_api_module_check(module, fn ^module ->
      {:ok, state} = ExApi.get_state
      api = state[module]
      if is_nil(api) do
        error = %ApiNotFoundError{module: module}
        {:error, Error.prepare(error)}
      else
        apply(success_callback, [api])
      end
    end)
  end

  @doc "Checks if passed implementation or its id is found/registered in state and matches specified api."
  @spec with_api_impl_check(Api.t, Implementation.t, function) :: term
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, WrongApiError.t}
  @spec with_api_impl_check(Api.t, {group :: atom, id :: atom} | id :: atom, function) :: term
        | {:error, ImplementationNotFoundError.t}
  def with_api_impl_check(
    api = %Api{},
    impl = %Implementation{},
    success_callback
  ) when is_function(success_callback) do
    cond do
      impl.api != api.module ->
        error = %WrongApiError{api: api, implementation: impl}
        {:error, Error.prepare(error)}
      is_nil(api.implementations[impl.group]) or
      is_nil(api.implementations[impl.group][impl.id]) ->
        error = %ImplementationNotRegisteredError{api: api, implementation: impl}
        {:error, Error.prepare(error)}
      impl != api.implementations[impl.group][impl.id] ->
        error = %ImplementationMismatchError{api: api, implementation: impl}
        {:error, Error.prepare(error)}
      true ->
        apply(success_callback, [impl])
    end
  end
  def with_api_impl_check(api = %Api{}, id_or_group_with_id, success_callback)
      when is_function(success_callback) do
    {group, id} = case id_or_group_with_id do
      {group, id} -> {group, id}
      id -> {Application.get_env(:ex_api, :default_groups, %{})[api.module], id}
    end
    group_map = api.implementations[group]
    if is_nil(group_map) or is_nil(group_map[id]) do
      error = %ImplementationNotFoundError{api: api, group: group, id: id}
      {:error, Error.prepare(error)}
    else
      apply(success_callback, [group_map[id]])
    end
  end

  @doc "Checks if specified module is loaded and is api."
  @spec with_api_module_check(Api.t | module, function) :: term
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def with_api_module_check(api = %Api{}, success_callback) when is_function(success_callback),
      do: with_api_module_check(api.module, success_callback)
  def with_api_module_check(module, success_callback) when is_atom(module) and is_function(success_callback) do
    with_module_check(module, fn ^module ->
      if is_nil(module.__info__(:exports)[:__api_info__]) do
        error = %NotApiError{module: module}
        {:error, Error.prepare(error)}
      else
        apply(success_callback, [module])
      end
    end)
  end

  @doc "Check if feature exists in implementation."
  @spec with_feature_check(Implementation.t, {name :: atom, arity :: non_neg_integer}, function) :: term
        | {:error, FeatureNotFoundError.t}
  def with_feature_check(impl, {name, arity}, success_callback)
      when is_atom(name) and is_integer(arity) and is_function(success_callback) do
    feature = impl.features[{name, arity}]
    if is_nil(feature) do
      error = %FeatureNotFoundError{arity: arity, implementation: impl, name: name}
      {:error, Error.prepare(error)}
    else
      apply(success_callback, [feature])
    end
  end

  @doc "Checkes if passed implementation or its module is loaded and registered. Then checks if it and its api is found in state."
  @spec with_impl_check(Implementation.t, boolean, function) :: term
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec with_impl_check(module, boolean, function) :: term
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  def with_impl_check(impl_or_id, for_register \\ false, success_callback)
  def with_impl_check(impl = %Implementation{}, for_register, success_callback) when is_function(success_callback) do
    with_api_check(impl.api, fn api ->
      module = impl.module
      with_impl_module_check(module, fn ^module -> do_with_impl_check(api, impl, for_register, success_callback) end)
    end)
  end
  def with_impl_check(module, for_register, success_callback) when is_atom(module) and is_function(success_callback) do
    with_impl_module_check(module, fn ^module ->
      module
      |> apply(:__impl_info__, [])
      |> with_impl_check(for_register, success_callback)
    end)
  end

  defp do_with_impl_check(api, impl, for_register, success_callback) do
    group = api.implementations[impl.group]
    if !for_register and (is_nil(group) or is_nil(group[impl.id])) do
      error = %ImplementationNotFoundError{api: api, group: impl.group, id: impl.id}
      {:error, Error.prepare(error)}
    else
      apply(success_callback, [impl])
    end
  end

  @doc "Checkes if passed implementation module is loaded and is registered. Then checks if it with specified group and its api is found in state."
  @spec with_impl_check(Implementation.t, atom, boolean, function) :: term
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec with_impl_check(module, atom, boolean, function) :: term
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  def with_impl_check(module, group, for_register, success_callback) do
    with_impl_module_check(module, fn ^module ->
      module
      |> apply(:__impl_info__, [])
      |> Map.put(:group, group)
      |> with_impl_check(for_register, success_callback)
    end)
  end

  @doc "Checks if specified module is loaded and is implementation."
  @spec with_impl_module_check(Implementation.t | module, function) :: term
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  def with_impl_module_check(impl = %Implementation{}, success_callback) when is_function(success_callback),
      do: with_impl_module_check(impl.module, success_callback)
  def with_impl_module_check(module, success_callback) when is_atom(module) and is_function(success_callback) do
    with_module_check(module, fn ^module ->
      if is_nil(module.__info__(:exports)[:__impl_info__]) do
        error = %NotImplementationError{module: module}
        {:error, Error.prepare(error)}
      else
        apply(success_callback, [module])
      end
    end)
  end

  @doc "Checks if specified module is loaded."
  @spec with_module_check(module, function) :: term | {:error, ModuleNotLoadedError.t}
  def with_module_check(module, success_callback)
      when is_atom(module) and is_function(success_callback) do
    if Code.ensure_loaded?(module) do
      apply(success_callback, [module])
    else
      error = %ModuleNotLoadedError{module: module}
      {:error, Error.prepare(error)}
    end
  end
end
