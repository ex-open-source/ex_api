defmodule ExApi do
  @moduledoc "This module manages all apis and theirs implementations."

  use GenServer

  alias ExApi.{Api, Implementation, ValidationHelper}
  alias ExApi.Error.{
    ApiMismatchError,
    ApiNotFoundError,
    ApiNotRegisteredError,
    FeatureNotFoundError,
    ImplementationMismatchError,
    ImplementationNotFoundError,
    ImplementationNotRegisteredError,
    ModuleNotLoadedError,
    NotApiError,
    NotImplementationError,
    WrongApiError
  }
  alias ExApi.Event.{
    ChangedDefaultImplementationEvent,
    RegisteredEvent,
    UnregisteredEvent
  }
  alias Phoenix.PubSub

  # call api

  @doc "Lists all registered apis."
  @spec all :: {:ok, list(Api.t)}
  def all, do: call_me(:all)

  @doc "Checks given api"
  @spec check_api(Api.t) :: {:ok, Api.t}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
  @spec check_api(module) :: {:ok, Api.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  defdelegate check_api(api_or_module), as: :get, to: __MODULE__

  @doc "Checks implementation with api."
  @spec check_api_impl(Api.t, Implementation.t) :: {:ok, Implementation.t}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, WrongApiError.t}
  @spec check_api_impl(Api.t, {group :: atom, id :: atom} | id :: atom) ::
        {:ok, Implementation.t}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec check_api_impl(module, Implementation.t)
        :: {:ok, Implementation.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  @spec check_api_impl(module, {group :: atom, id :: atom} | id :: atom) ::
        {:ok, Implementation.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  defdelegate check_api_impl(api_or_module, impl_or_id_or_group_with_id), as: :get_impl, to: __MODULE__

  @doc "Checks implementation."
  @spec check_impl(Implementation.t) :: {:ok, Implementation.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec check_impl(module) :: {:ok, Implementation.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  defdelegate check_impl(impl_or_module), as: :get_impl, to: __MODULE__

  @doc "Fetches api."
  @spec get(Api.t) :: {:ok, Api.t}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
  @spec get(module) :: {:ok, Api.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def get(api_or_module), do: ValidationHelper.with_api_check(api_or_module, &call_me({:get, &1}))

  @doc "Fetches default implementation for specified api."
  @spec get_default_impl(Api.t) :: {:ok, Implementation.t | nil}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
  @spec get_default_impl(module) :: {:ok, Implementation.t | nil}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def get_default_impl(api_or_module),
      do: ValidationHelper.with_api_check(api_or_module, &call_me({:get_default_impl, &1}))

  @doc "Fetches fallback implementation for specified api."
  @spec get_fallback_impl(Api.t) :: {:ok, Implementation.t | nil}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
  @spec get_fallback_impl(module) :: {:ok, Implementation.t | nil}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def get_fallback_impl(api_or_module),
      do: ValidationHelper.with_api_check(api_or_module, &call_me({:get_fallback_impl, &1}))

  @doc "Check support of feature for specific api and implementation."
  @spec get_feature_support(Api.t, Implementation.t, {name :: atom, arity :: non_neg_integer})
        :: {:ok, Feature.support}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
        | {:error, FeatureNotFoundError.t}
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, WrongApiError.t}
  @spec get_feature_support(Api.t, {group :: atom, id :: atom} | id :: atom, {name :: atom, arity :: non_neg_integer})
        :: {:ok, Feature.support}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
        | {:error, FeatureNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec get_feature_support(module, Implementation.t, {name :: atom, arity :: non_neg_integer})
        :: {:ok, Feature.support}
        | {:error, ApiNotFoundError.t}
        | {:error, FeatureNotFoundError.t}
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  @spec get_feature_support(module, {group :: atom, id :: atom} | id :: atom, {name :: atom, arity :: non_neg_integer})
        :: {:ok, Feature.support}
        | {:error, ApiNotFoundError.t}
        | {:error, FeatureNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def get_feature_support(api_or_module, impl_or_id_or_group_with_id, feature) do
    ValidationHelper.with_api_check(api_or_module, fn api ->
      ValidationHelper.with_api_impl_check(api, impl_or_id_or_group_with_id, fn impl ->
        ValidationHelper.with_feature_check(impl, feature, &call_me({:get_feature_support, &1}))
      end)
    end)
  end

  @doc "Check support of feature for specific implementation."
  @spec get_feature_support(Implementation.t, {name :: atom, arity :: non_neg_integer}) :: {:ok, Feature.support}
        | {:error, ApiNotFoundError.t}
        | {:error, FeatureNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec get_feature_support(module, {name :: atom, arity :: non_neg_integer}) :: {:ok, Feature.support}
        | {:error, ApiNotFoundError.t}
        | {:error, FeatureNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  def get_feature_support(impl_or_module, feature) do
    ValidationHelper.with_impl_check(impl_or_module, fn impl ->
      ValidationHelper.with_feature_check(impl, feature, &call_me({:get_feature_support, &1}))
    end)
  end

  @doc "Fetches implementation for specified group and api."
  @spec get_impl(Api.t, Implementation.t) :: {:ok, Implementation.t}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, WrongApiError.t}
  @spec get_impl(Api.t, {group :: atom, id :: atom} | id :: atom) :: {:ok, Implementation.t}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec get_impl(module, Implementation.t) :: {:ok, Implementation.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  @spec get_impl(module, {group :: atom, id :: atom} | id :: atom) :: {:ok, Implementation.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def get_impl(api_or_module, impl_or_id_or_group_with_id) do
    ValidationHelper.with_api_check(api_or_module, fn api ->
      ValidationHelper.with_api_impl_check(api, impl_or_id_or_group_with_id, &call_me({:get_impl, &1}))
    end)
  end

  @doc "Fetches implementation."
  @spec get_impl(Implementation.t) :: {:ok, Implementation.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec get_impl(module) :: {:ok, Implementation.t}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  def get_impl(impl_or_module), do: ValidationHelper.with_impl_check(impl_or_module, &call_me({:get_impl, &1}))

  @doc "Fetches all implementations for specified api."
  @spec get_impls(Api.t) :: {:ok, list(Implementation.t)}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
  @spec get_impls(module) :: {:ok, list(Implementation.t)}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def get_impls(api_or_module), do: ValidationHelper.with_api_check(api_or_module, &call_me({:get_impls, &1}))

  @doc "Returns server state"
  @spec get_state :: {:ok, %{optional(module) => Api.t}}
  def get_state, do: call_me(:get_state)

  # init part

  @default_groups Application.get_env(:ex_api, :default_groups, %{})
  @default_impls Application.get_env(:ex_api, :default_impls, %{})

  @doc "Callback implementation of: `c:GenServer.init/1`."
  @spec init(args :: term) ::
    {:ok, state} |
    {:ok, state, timeout | :hibernate} |
    :ignore |
    {:stop, reason :: any}
    when state: any
  def init([]) do
    apis =
      :ex_api
      |> Application.get_env(:apis, [])
      |> Enum.map(&init_api/1)
      |> Enum.into(%{})
    PubSub.subscribe(:ex_api, "events")
    {:ok, apis}
  end

  defp init_api({module, modules}) when is_atom(module) and is_list(modules) do
    impls =
      modules
      |> Enum.map(&init_impl/1)
      |> Enum.reduce(%{}, &group_impl/2)
    old_pid = GenServer.whereis(module)
    pid = if is_nil(old_pid) do
      {:ok, new_pid} = module.start_link
      new_pid
    else
      old_pid
    end
    info = apply(module, :__api_info__, [])
    default_impl = Map.get(@default_impls, module, {@default_groups[module], nil})
    {
      module,
      %Api{
        default_implementation: default_impl,
        doc: info.doc,
        features: info.features,
        implementations: impls,
        line: info.line,
        module: module,
        pid: pid
      }
    }
  end

  defp init_impl(module), do: apply(module, :__impl_info__, [])

  defp group_impl(impl, acc) do
    if Map.has_key?(acc, impl.group) do
      put_in(acc[impl.group][impl.id], impl)
    else
      put_in(acc[impl.group], %{impl.id => impl})
    end
  end

  # cast api

  @doc "Registers api asynchronously."
  @spec register_api(Api.t | module) :: {:ok, pid}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def register_api(api_or_module),
      do: ValidationHelper.with_api_module_check(api_or_module, &cast_me({:register_api, &1}))

  @doc "Registers api fallback implementation asynchronously."
  @spec register_fallback_impl(Implementation.t) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, NotApiError.t}
  @spec register_fallback_impl(module) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
        | {:error, NotImplementationError.t}
  def register_fallback_impl(impl_or_module),
      do: ValidationHelper.with_impl_check(impl_or_module, true, &cast_me({:register_fallback_impl, &1}))

  @doc "Registers api implementation asynchronously."
  @spec register_impl(Implementation.t) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, NotApiError.t}
  @spec register_impl(module) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
        | {:error, NotImplementationError.t}
  def register_impl(impl_or_module),
      do: ValidationHelper.with_impl_check(impl_or_module, true, &cast_me({:register_impl, &1}))

  @doc "Registers api implementation asynchronously."
  @spec register_impl(Implementation.t | module, atom) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
        | {:error, NotImplementationError.t}
  def register_impl(impl_or_module, group) when is_atom(group),
      do: ValidationHelper.with_impl_check(impl_or_module, true, &cast_me({:register_impl, &1, group}))

  @doc "Changes default api implementation asynchronously."
  @spec set_default_impl(Api.t, Implementation.t) :: {:ok, pid}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, WrongApiError.t}
  @spec set_default_impl(Api.t, {group :: atom, id :: atom} | id :: atom) :: {:ok, pid}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec set_default_impl(module, Implementation.t) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationMismatchError.t}
        | {:error, ImplementationNotRegisteredError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  @spec set_default_impl(module, {group :: atom, id :: atom} | id :: atom) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def set_default_impl(api_or_module, impl_or_id_or_group_with_id) do
    ValidationHelper.with_api_check(api_or_module, fn checked_api ->
      ValidationHelper.with_api_impl_check(checked_api, impl_or_id_or_group_with_id, &cast_me({:set_default_impl, &1}))
    end)
  end

  @doc "Changes default api implementation asynchronously."
  @spec set_default_impl(Implementation.t) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec set_default_impl(module) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  def set_default_impl(impl_or_module),
      do: ValidationHelper.with_impl_check(impl_or_module, &cast_me({:set_default_impl, &1}))

  @doc "Starts ExApi server."
  @spec start_link([]) :: GenServer.on_start
  def start_link(_args \\ []), do: GenServer.start_link(__MODULE__, [], name: __MODULE__)

  @doc "Stops ExApi and all apis servers."
  @spec stop :: :ok
  def stop do
    PubSub.unsubscribe(:ex_api, "events")
    pid = GenServer.whereis(ExApi)
    {:ok, apis} = ExApi.all
    Enum.each(apis, &(GenServer.stop(&1.pid)))
    GenServer.stop(pid)
  end

  @doc "Unregisters api asynchronously."
  @spec unregister_api(Api.t) :: {:ok, pid}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
  @spec unregister_api(module) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def unregister_api(api_or_module),
      do: ValidationHelper.with_api_check(api_or_module, &cast_me({:unregister_api, &1}))

  @doc "Unregisters api fallback asynchronously."
  @spec unregister_fallback_impl(Api.t) :: {:ok, pid}
        | {:error, ApiMismatchError.t}
        | {:error, ApiNotRegisteredError.t}
  @spec unregister_fallback_impl(module) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotApiError.t}
  def unregister_fallback_impl(api_or_module),
      do: ValidationHelper.with_api_check(api_or_module, &cast_me({:unregister_fallback_impl, &1}))

  @doc "Unregisters api implementation asynchronously."
  @spec unregister_impl(Implementation.t) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec unregister_impl(module) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  def unregister_impl(impl_or_module),
      do: ValidationHelper.with_impl_check(impl_or_module, &cast_me({:unregister_impl, &1}))

  @doc "Unregisters api implementation asynchronously."
  @spec unregister_impl(Implementation.t, atom) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
  @spec unregister_impl(module, atom) :: {:ok, pid}
        | {:error, ApiNotFoundError.t}
        | {:error, ImplementationNotFoundError.t}
        | {:error, ModuleNotLoadedError.t}
        | {:error, NotImplementationError.t}
  def unregister_impl(impl_or_module, group) when is_atom(group),
      do: ValidationHelper.with_impl_check(impl_or_module, group, false, &cast_me({:unregister_impl, &1, group}))

  # helpers

  defp call_me(request), do: {:ok, GenServer.call(__MODULE__, request)}
  defp cast_me(request) do
    status = :ok = GenServer.cast(__MODULE__, request)
    {status, GenServer.whereis(__MODULE__)}
  end

  # call handlers

  def handle_call(request, _from, state), do: {:reply, do_call(request, state), state}

  defp do_call(:all, state), do: Map.values(state)
  defp do_call({:get, api}, _state), do: api
  defp do_call({:get_default_impl, api}, _state) do
    {group_id, impl_id} = api.default_implementation
    group = api.implementations[group_id]
    if is_nil(group), do: nil, else: group[impl_id]
  end
  defp do_call({:get_fallback_impl, api}, _state), do: api.fallback_implementation
  defp do_call({:get_feature_support, feature_support}, _state), do: feature_support
  defp do_call({:get_impl, impl}, _state), do: impl
  defp do_call({:get_impls, api}, _state), do: api.implementations
  defp do_call(:get_state, state), do: state

  # cast handlers

  def handle_cast(request, apis) do
    {state, event} = do_cast(request, apis)
    if event, do: PubSub.broadcast_from(:ex_api, self(), "events", event)
    {:noreply, state}
  end

  defp do_cast({:register_api, module}, apis) when is_atom(module) do
    {^module, api} = init_api({module, []})
    state = Map.put(apis, api.module, api)
    event = %RegisteredEvent{again: Map.has_key?(apis, module), data: api}
    {state, event}
  end
  defp do_cast({:register_fallback_impl, impl}, apis) do
    state = put_in(apis[impl.api].fallback_implementation, impl)
    event = %RegisteredEvent{again: is_nil(apis[impl.api].fallback_implementation), data: impl, fallback: true}
    {state, event}
  end
  defp do_cast({:register_impl, impl}, apis) do
    add_impl = &(if is_nil(&1), do: %{&2.id => &2}, else: Map.put(&1, &2.id, &2))
    state = update_in(apis[impl.api].implementations[impl.group], &add_impl.(&1, impl))
    group = apis[impl.api].implementations[impl.group]
    again = not(is_nil(group) or is_nil(group[impl.id]))
    event = %RegisteredEvent{again: again, data: impl}
    {state, event}
  end
  defp do_cast({:register_impl, impl, group}, apis), do: do_cast({:register_impl, %{impl | group: group}}, apis)
  defp do_cast({:set_default_impl, impl}, apis) do
    api = apis[impl.api]
    state = put_in(
      apis[api.module].default_implementation,
      {impl.group, impl.id}
    )
    event = %ChangedDefaultImplementationEvent{api: state[api.module], implementation: impl}
    {state, event}
  end
  defp do_cast({:unregister_api, api}, apis) do
    state = Map.delete(apis, api.module)
    event = %UnregisteredEvent{data: apis[api.module]}
    {state, event}
  end
  defp do_cast({:unregister_fallback_impl, api}, apis) do
    if api.fallback_implementation do
      {
        put_in(apis[api.module].fallback_implementation, nil),
        %UnregisteredEvent{data: api, fallback: true}
      }
    else
      {apis, nil}
    end
  end
  defp do_cast({:unregister_impl, impl}, apis) do
    {^impl, state} =
      pop_in(apis[impl.api].implementations[impl.group][impl.id])
    event = %UnregisteredEvent{data: impl}
    {state, event}
  end
  defp do_cast({:unregister_impl, impl, group}, apis), do: do_cast({:unregister_impl, %{impl | group: group}}, apis)

  # info handler

  def handle_info(event, apis) do
    {state, _new_event} =
      event
      |> request_from_event(apis)
      |> do_cast(apis)
    {:noreply, state}
  end

  defp request_from_event(%ChangedDefaultImplementationEvent{implementation: impl}, _state),
       do: {:set_default_impl, impl}
  defp request_from_event(%RegisteredEvent{data: %Api{module: module}}, _state), do: {:register_api, module}
  defp request_from_event(%RegisteredEvent{data: data = %Implementation{}, fallback: true}, _state),
       do: {:register_fallback_impl, data}
  defp request_from_event(%RegisteredEvent{data: data = %Implementation{}}, _state), do: {:register_impl, data}
  defp request_from_event(%UnregisteredEvent{data: data = %Api{}, fallback: true}, _state),
       do: {:unregister_fallback_impl, data}
  defp request_from_event(%UnregisteredEvent{data: data = %Api{}}, _state), do: {:unregister_api, data}
  defp request_from_event(%UnregisteredEvent{data: data = %Implementation{}}, _state), do: {:unregister_impl, data}
end
